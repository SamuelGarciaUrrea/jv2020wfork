package userAccess;

public class userAccessException extends RuntimeException {

	public userAccessException() {
		super();
	}

	public userAccessException(String message) {
		super(message);
	}

}
